# PiVPN

Aujourd'huit nous allons mettre en place un serveur VPN sur une rasberry Pi, ici nous partirons plutot sur wireguard, ce protocole tout récent a pris le monde du VPN par surprise. On relève notamment un développement éclair, des performances surprenantes, sans oublier son intégration au très select Kernel Linux. Avec autant d'arguments en sa faveur, il a vite trouvé preneur chez de nombreux éditeurs de VPN qui n'ont pas manqué d'intégrer le protocole dans leurs services.

PiVPN a pour objectif de faciliter la configuration et la gestion d’un serveur VPN depuis une Raspberry Pi. Je l’ai configuré rapidement sur ma Raspberry Pi, mes appareils étaient paramétrés pour l’utiliser. Le moins que l’on puisse dire, c’est qu’il est très efficace ! et incroyablement rapide !

## Pour commencer

Sachez que le client WireGuard est disponible dans la partie kaisen-linux notamment ici :
https://gitlab.com/roystraque/kaisen-linux-utility


## Installation

```
curl -L https://gitlab.com/roystraque/pivpn/raw/main/install.sh | bash

```
Et c’est tout. Rien de plus, rien de moins. L’outil va réaliser différentes vérifications sur votre Raspberry Pi et lancer automatiquement l’installation des différents paquets.

## Configuration

La configuration est assez simple, il suffit de suivre le guide en anglais. Oui, cela peut être un frein pour certains… tout est en anglais. Alors je vous rassure, cela reste tout à fait accessible. Il n’y a rien de vraiment très compliqué.On choisit si le Raspberry Pi est en DHCP ou IP fixe (à favoriser pour la redirection sur votre Box), puis on va sélectionner l’utilisateur (User) qui pourra gérer serveur VPN.

![PiVPN Vue01](img/pi01.webp)


Dans mon cas, je n’ai que l’utilisateur pi de paramétrer sur mon matériel. Arrive ensuite le mode d’installation, c’est maintenant qu’il faut choisir le type de service VPN que vous souhaitez mettre en place OpenVPN ou WireGuard. Je suis un grand fan d’OpenVPN, mais il est gourmand en ressource (voire lent). Depuis plusieurs mois maintenant, je n’utilise que WireGuard et c’est un vrai bonheur. C’est celui que j’utilise pour ce tuto…


![PiVPN Vue02](img/pi02.webp)


On a quasiment fini, il faut indiquer le port à utiliser/rediriger vers votre Raspberry Pi. Dans notre cas, le port par défaut pour WireGuard est 51820 (UDP). Avant-dernier écran, on vous demande de sélectionner un fournisseur de DNS pour les clients. Si vous utilisez AdGuard ou PiHole sur votre installation réseau pour bloquer les publicités, c’est le moment de sélectionner Custom (tout en bas de la liste) et de saisir son nom ou son adresse IP. Vous pourrez en mettre 2 en les séparant par une virgule.


![PiVPN Vue03](img/pi03.webp)


Enfin, il est demandé de choisir le moyen de connexion à votre serveur VPN : via l’adresse IP publique de votre Box ou avec une entrée DNS (Domaine/Dynamic DNS). Si vous choisissez la seconde solution, il faudra le nom (ex. : domaine.fr/.com/.org…). Voila, c’est terminé pour l’installation et la configuration de votre VPN avec PiVPN.


![PiVPN Vue04](img/pi04.webp)

Un petit message vous recommandera d’activer les Unattended Upgrades (les mises à jour de sécurité). Enfin, il est recommandé de redémarrer son Raspberry Pi après l’installation de PiVPN.


![PiVPN Vue05](img/pi05.webp)


## Ajout d’un utilisateur/périphérique

Ensuite pour ajouter un appareil, il faudra taper la commande :

```
pivpn -a
```

## Creation vpn iphone fx - PiVPN : OpenVPN ou WireGuard sur un Raspberry Pi
et le nom du client. Il ne vous reste plus qu’à installer l’application WireGuard sur votre téléphone ou ordinateur 😉


## Récupérer le QRcode
Pour récupérer le QRcode pour l’application téléphone ou tablette, vous taperez la commande :

```
pivpn -qr
```


## Lister les utilisateurs/périphériques

La commande ci-dessous vous permettra de lister tous les clients/périphériques ajoutés précédemment :

```
pivpn -l
```

## Pour voir les utilisateurs/clients connectés, tapez :

```
pivpn -c
```

## Désinstaller PiVPN
Si vous souhaitez désinstaller PiVPN, le programme est simple et assez efficace (attention, il y a un redémarrage du Raspberry Pi à faire) :

```
pivpn -u
```

## Fichiers de configurations

Sachez que les fichiers de configuration pour vos appareils (ordinateur, tablette, smartphone…) sont tous disponibles depuis le dossier

```
/home/pi/configs (par défaut).
```

## Help PiVPN

Voilà finalement à quoi ressemblera la gestion de votre serveur

![PiVPN Vue06](img/pi06.png)


## Filtrage IP LAN

Petites informations supplémentaires pour sécuriser votre VPN en local, il y a possibilité de mettre un filtrage IP afin de donnée accès à des services intranet de votre réseau de façon spécifique pour éviter que l'utilisateur ait accès à tout, pour ça je vous ai mis un document PDF de notre amie IT-Connect, qui explique comment faire cela.


```
sudo nano /etc/ufw/before.rules (fichier à modifier).

```

## Lien supplémentaire

https://www.journalduhacker.net
https://www.cachem.fr/vpn-wireguard-securiser/
https://www.it-connect.fr/mise-en-place-de-wireguard-vpn-sur-debian-11/
https://korben.info/comment-installer-le-vpn-wireguard-facilement.html
https://www.wireguard.com/install/






